import React, { Component } from 'react'
import { Modal } from 'antd';
import AddUserForm from './UM_AddUserForm'
import UpdateUserForm from "./UM_UpdateUserForm"

export default class UM_Modal extends Component {
    render() {
        return (
            <Modal
            centered={true}
            title={this.props.title}
            visible={this.props.visible}
            footer={null}
            onCancel={this.props.onCancel}
          >
         
           { this.props.formType === "add" ? <AddUserForm modalclose={this.props.visible}/> : null}
           { this.props.formType === "update" ? 
           (
           <UpdateUserForm 
                username ={this.props.username}
                email={this.props.email}
                id={this.props.id}
                modalclose={this.props.visible}
           /> 
           )
           : null}
          </Modal>
        )
    }
}
