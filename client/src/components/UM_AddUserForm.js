import React, { useState , useEffect } from 'react'
import { Form, Input, Button, Select , Alert , message} from 'antd';
import { useDispatch, useSelector } from "react-redux";
import {addUser} from "../redux/actions/usersAction" 
import  axios from 'axios';

const { Option } = Select;



function UM_AddUserForm(props) {

    const dispatch = useDispatch();


    useEffect(() => {
       if(props.modalclose === true){
        form.resetFields();
        } 
    }, [props.modalclose]); 

    const [form] = Form.useForm();

    const onFinish =(value)=>{
       console.log(value)
       dispatch(addUser(value))
       message.success("user added in database")
    }

    return (
        <Form
            className="user-manager-form"
            onFinish={onFinish}
            form={form}
        >
            <Form.Item
                className="form-item"
                label="Username"
                name="username"
                rules={[{ required: true, message: 'Please input username!' }]}
            >
                <Input  className="input" />
            </Form.Item>

            <Form.Item
                className="form-item"
                label="Email"
                name="email"
                rules={[{ required: true, message: 'Please input email!' }]}
            >
                <Input  className="input" />
            </Form.Item>


            <Form.Item
               className="form-item"
                label="Password"
                name="password"
                rules={[{ required: true, message: 'Please input your password!' }]}
            >
                <Input.Password className="input"  />
            </Form.Item>

            <Form.Item >
                <Button type="primary" className="button" htmlType="submit">
                Add User
                </Button>
            </Form.Item>
            
        </Form>
    )
}

export default UM_AddUserForm
