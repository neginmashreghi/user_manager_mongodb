const express = require("express");
const bodyParser = require('body-parser')
const mongoose = require("mongoose");
const usersRouter = require("./routers/user");
const cors = require('cors');


// App Variables
const app = express()
var http = require('http').createServer(app);
var io = require('socket.io')(http);
const port = process.env.PORT || "8000";

// Database config and connect
const mongoURI = "mongodb+srv://Negin:Negin123@cluster0.gvo0c.mongodb.net/usermanager?retryWrites=true&w=majority"
mongoose.connect(mongoURI, {
    useNewUrlParser: true,
    useFindAndModify: false,
    useCreateIndex: true,
    useUnifiedTopology: true 
})

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection Error:'));
db.on('open', () => {
  console.log("MongoDB successfully connected")
  const userCollection = db.collection('users');
  // watch all the changees happeedn in users collection
  const changeStreamUser = userCollection.watch();
  changeStreamUser.on('change', (change) => {
    io.emit('renderTableComponenet');
  })
});


//  App Configuration
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors());


// Routes Definitions
app.use("/api/users", usersRouter);


// Server Activation
http.listen(port, () => {
    console.log(`Listening to requests on http://localhost:${port}`);
});