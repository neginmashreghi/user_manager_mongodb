const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const UserModal = require("../modals/User");

router.get("/test", (req, res) => res.json({ msg: "Users Router Works" }));

router.get("/all", async (req, res) => {
    let allUsers = await UserModal.find();
    res.json(allUsers);
});

router.delete('/delete', async (req, res, next) => {
    console.log(req.body)
   
    UserModal.findOneAndDelete({email: req.body.email })
      .then(doc => {
        res.json(doc);
      })
      .catch(err => {
        return res.status(400).json({ error: "error in deleting a user" });
      })   
   
});

router.put('/update', async (req, res, next) => {

   
        const newData={
            username: req.body.username,
            email: req.body.email, 
        }
        UserModal.findOneAndUpdate({_id:req.body.id }, newData, {new: true})
        .then(doc => {     
            res.json(doc);
        })
        .catch(err => {
            return res.status(400).json({ error: "error in updating user" });
        })  
   
   

     
    
});

router.post("/add", async (req, res) => {
      
    let user = await UserModal.findOne({ email: req.body.email });
    if (user) {
      return res.status(400).json({ email: "Email already exists" });
    } else {
        
        let newUser = new UserModal({
            username: req.body.username,
            email: req.body.email,
            password: req.body.password
          });

        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(newUser.password, salt, async (err, hash) => {
            if (err) throw err;
            newUser.password = hash;
            let user;
            try {
                user = await newUser.save();
            } catch (err) {
                console.log(err);
                return res.json({ err });
            }
            return res.json(user);
            });
        });  
    } 
});


module.exports = router;